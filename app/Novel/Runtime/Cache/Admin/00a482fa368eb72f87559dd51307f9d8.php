<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
<title>假使明天不再来临</title>
<style>
	input {background-color:#FFFFE1}
	.STYLE1 {font-size: 12px}
	.STYLE4 {
		font-size: 12px;
		color: #1F4A65;
		font-weight: bold;
	}
	
	a:link {
		color: #06482a;
		text-decoration: none;
	
	}
	a:visited {
		color: #06482a;
		text-decoration: none;
	}
	a:hover {
		color: #FF0000;
		text-decoration: underline;
	}
	a:active {
		color: #FF0000;
		text-decoration: none;
	}
	.STYLE7 {font-size: 12}
	textarea{overflow:auto}
</style>
</head>
<body style="background-color:#F2F0F0;margin:auto;">
	<div style="margin:auto;margin-top:30px;width:100%;height:30px;background-color:#CCFF99;line-height:30px;">
		<div style="width:960px;margin:auto;">
			<div style="float:left;width:200px;font-size:20px;">
				<a style="text-decoration:none;" href="<?php echo U('Admin/Index/index');?>">写故事</a>
			</div>
			<div style="float:left;width:200px;font-size:20px;">
				<a style="text-decoration:none;" href="<?php echo U('Admin/Index/novellist');?>">故事列表</a>
			</div>
			<div style="float:left;width:200px;font-size:20px;">
				<a href="<?php echo U('Admin/Index/book');?>" style="text-decoration:none;">预定统计</a>
			</div>
			<div style="float:right;width:100px;font-size:20px;">
				<a style="text-decoration:none;" href="<?php echo U('Admin/Index/logout');?>">退出</a>
			</div>
		</div>
	</div>	
	<div style="clear:both;"></div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>跳转提示</title>
<style type="text/css">
*{ padding: 0; margin: 0; }
body{ background: #fff; font-family: '微软雅黑'; color: #333; font-size: 16px; }
.system-message{ padding: 24px 48px; }
.system-message h1{ font-size: 100px; font-weight: normal; line-height: 120px; margin-bottom: 12px; }
.system-message .jump{ padding-top: 10px}
.system-message .jump a{ color: #333;}
.system-message .success,.system-message .error{ line-height: 1.8em; font-size: 36px }
.system-message .detail{ font-size: 12px; line-height: 20px; margin-top: 12px; display:none}
</style>
</head>
<body>
<div class="system-message">
<?php if(isset($message)): ?><h1>:)</h1>
<p class="success"><?php echo($message); ?></p>
<?php else: ?>
<h1>:(</h1>
<p class="error"><?php echo($error); ?></p><?php endif; ?>
<p class="detail"></p>
<p class="jump">
页面自动 <a id="href" href="<?php echo($jumpUrl); ?>">跳转</a> 等待时间： <b id="wait"><?php echo($waitSecond); ?></b>
</p>
</div>
<script type="text/javascript">
(function(){
var wait = document.getElementById('wait'),href = document.getElementById('href').href;
var interval = setInterval(function(){
	var time = --wait.innerHTML;
	if(time <= 0) {
		location.href = href;
		clearInterval(interval);
	};
}, 1000);
})();
</script>
</body>
</html>

</body>
</html>