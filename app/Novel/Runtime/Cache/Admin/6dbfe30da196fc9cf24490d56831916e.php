<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
<title>假使明天不再来临</title>
<style>
	input {background-color:#FFFFE1}
	.STYLE1 {font-size: 12px}
	.STYLE4 {
		font-size: 12px;
		color: #1F4A65;
		font-weight: bold;
	}
	
	a:link {
		color: #06482a;
		text-decoration: none;
	
	}
	a:visited {
		color: #06482a;
		text-decoration: none;
	}
	a:hover {
		color: #FF0000;
		text-decoration: underline;
	}
	a:active {
		color: #FF0000;
		text-decoration: none;
	}
	.STYLE7 {font-size: 12}
	textarea{overflow:auto}
</style>
</head>
<body style="background-color:#F2F0F0;margin:auto;">
	<div style="margin:auto;margin-top:30px;width:100%;height:30px;background-color:#CCFF99;line-height:30px;">
		<div style="width:960px;margin:auto;">
			<div style="float:left;width:200px;font-size:20px;">
				<a style="text-decoration:none;" href="<?php echo U('Admin/Index/index');?>">写故事</a>
			</div>
			<div style="float:left;width:200px;font-size:20px;">
				<a style="text-decoration:none;" href="<?php echo U('Admin/Index/novellist');?>">故事列表</a>
			</div>
			<div style="float:left;width:200px;font-size:20px;">
				<a href="<?php echo U('Admin/Index/book');?>" style="text-decoration:none;">预定统计</a>
			</div>
			<div style="float:right;width:100px;font-size:20px;">
				<a style="text-decoration:none;" href="<?php echo U('Admin/Index/logout');?>">退出</a>
			</div>
		</div>
	</div>	
	<div style="clear:both;"></div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:30px;">
	  <tr>
	    <td style="height:30px;">
	    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		      	<tr>
			        <td style="width:15px; height:30px">
			        	<img src="__PUBLIC__/Images/tab_03.gif" style="width:15px; height:30px;" />
			        </td>
			        <td width="99%" background="__PUBLIC__/Images/tab_05.gif" style="height:30px;">
			        	<span class="STYLE4">故事列表</span>
			        </td>
			        <td style="width:14px;">
			        	<img src="__PUBLIC__/Images/tab_07.gif" style="width:14px;height:30px;" />
			        </td>
	      		</tr>
	      	</table>
	      </td>
	  </tr>
	  <tr>
	    <td>
	    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	        	<tr>
	        		<td width="9" background="__PUBLIC__/Images/tab_12.gif">&nbsp;</td>
	        		<td bgcolor="#f3ffe3">
	        			<table width="99%" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#c0de98" onmouseover="changeto()"  onmouseout="changeback()">
	         				 <tr>
	            				<td width="6%" height="26" background="__PUBLIC__/Images/tab_14.gif" class="STYLE1"><div align="center" class="STYLE2 STYLE1">序号</div></td>
	            				<td width="16%" height="26" background="__PUBLIC__/Images/tab_14.gif" class="STYLE1"><div align="center" class="STYLE2 STYLE1">故事名</div></td>
	            				<td width="20%" height="18" background="__PUBLIC__/Images/tab_14.gif" class="STYLE1"><div align="center" class="STYLE2 STYLE1">上线日期</div></td>
	            				<td width="24%" height="18" background="__PUBLIC__/Images/tab_14.gif" class="STYLE1"><div align="center" class="STYLE2 STYLE1">上传日期</div></td>
	            				<td width="10%" height="18" background="__PUBLIC__/Images/tab_14.gif" class="STYLE1"><div align="center" class="STYLE2 STYLE1">评论</div></td>
	            				<td width="10%" height="18" background="__PUBLIC__/Images/tab_14.gif" class="STYLE1"><div align="center" class="STYLE2 STYLE1">喜欢数量</div></td>
	            				<td width="10%" height="18" background="__PUBLIC__/Images/tab_14.gif" class="STYLE1"><div align="center" class="STYLE2 STYLE1">雷人数量</div></td>
	            				<td width="10%" height="18" background="__PUBLIC__/Images/tab_14.gif" class="STYLE1"><div align="center" class="STYLE2 STYLE1">操作</div></td>
	          				 </tr>
					         <?php if(is_array($novellist)): foreach($novellist as $key=>$v): ?><tr>
					            <td height="18" bgcolor="#FFFFFF" class="STYLE2"><div align="center" class="STYLE2 STYLE1"><?php echo ($v["id"]); ?></div></td>
					            <td height="18" bgcolor="#FFFFFF" class="STYLE2"><div align="center" class="STYLE2 STYLE1">#<?php echo ($v["release_date"]); ?>故事#</div></td>
					            <td height="18" bgcolor="#FFFFFF"><div align="center" class="STYLE2 STYLE1"><?php echo ($v["release_date"]); ?></div></td>
					            <td height="18" bgcolor="#FFFFFF"><div align="center" class="STYLE2 STYLE1"><?php echo (date("Y-m-d H:i:s",$v["update_time"])); ?></div></td>
					            <td height="18" bgcolor="#FFFFFF">
						            <div align="center" class="STYLE2 STYLE1">
						            	<a style="text-decoration:none" href="__URL__/comment/id/<?php echo ($v["id"]); ?>">评论列表(<?php echo ($v["comment_counts"]); ?>)</a>
						            </div>
					            </td>
					            <td height="18" bgcolor="#FFFFFF"><div align="center" class="STYLE2 STYLE1"><?php echo ($v["like_count"]); ?></div></td>
					            <td height="18" bgcolor="#FFFFFF"><div align="center" class="STYLE2 STYLE1"><?php echo ($v["ray_count"]); ?></div></td>
					           	<td height="18" bgcolor="#FFFFFF">
					           		<div align="center" class="STYLE2 STYLE1">
					           			<a style="text-decoration:none;" href="__URL__/edit/id/<?php echo ($v["id"]); ?>">编辑</a>
					           		</div>
					           	</td>
					         </tr><?php endforeach; endif; ?>
	       			 	</table>
	        		</td>
	        		<td width="9" background="__PUBLIC__/Images/tab_16.gif">&nbsp;</td>
	      		</tr>
	    	</table>
	  	</td>
	  </tr>
	  <tr>
	    <td height="29">
	    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		      <tr>
		        <td width="15" height="29"><img src="__PUBLIC__/Images/tab_20.gif" width="15" height="29" /></td>
		        <td background="__PUBLIC__/Images/tab_21.gif">
			        <table width="100%" border="0" cellspacing="0" cellpadding="0">
			          <tr>
			            <td width="25%" height="29" nowrap="nowrap">
			            	<span class="STYLE1">共<?php echo ($counts); ?>条记录</span>
			            </td>
			          </tr>
			        </table>
		        </td>
		        <td width="14"><img src="__PUBLIC__/Images/tab_22.gif" width="14" height="29" /></td>
		      </tr>
	    	</table>
	    </td>
	  </tr>
	</table>



</body>
</html>