<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
		<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1" />
		<script type="text/javascript" src="__PUBLIC__/Js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="__PUBLIC__/Js/shareto.js"></script>	
		<link href="__PUBLIC__/Css/zhanghui/frontend.css" type=text/css rel=stylesheet />
		<script type="text/javascript"  src="http://qzonestyle.gtimg.cn/qzone/app/qzlike/qzopensl.js#jsdate=20111201" charset="utf-8"></script>
		<script type="text/javascript">
			document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
	    	WeixinJSBridge.call('showToolbar');
			});	

			var novel_zh = '<?php echo getImageUrl("novel_zh.jpg");?>';
		</script>

	<title>假使明天不再来临</title>
</head>
<body style="width:95%;margin:auto;font-family:'microsoft yahei',Arial,宋体;font-size:62.5%;">
	<div style="padding:0.5em;font-size:2.5em;">作者是这样一个人... ...</div>
<div style="font-size:1.8em;line-height:1.5em;border-top:0.1em solid lightgrey;border-bottom:0.1em solid lightgrey;padding:0.5em;">
<div>作者不是职业作家，但写作，是作者从未放下的梦想。</div>
<div>一直想写长篇，但开了很多个头，总不满意，无以为继，只好搁置。</div>
<div>所以先写些短篇，是为练笔，也是不使光阴虚度。</div>
<div>一个短小的故事，不会改变什么，只可能触动当下的一点点情肠。</div>
<div>让你想起生命中一个同样短小而触动的瞬间，或一蹙眉，或一浅笑，就已足够。</div>
</div>
<div style="padding-top:1.5em;"></div>

</body>
</html>