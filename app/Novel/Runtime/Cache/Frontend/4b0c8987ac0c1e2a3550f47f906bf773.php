<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
		<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1" />
		<script type="text/javascript" src="__PUBLIC__/Js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="__PUBLIC__/Js/shareto.js"></script>	
		<link href="__PUBLIC__/Css/zhanghui/frontend.css" type=text/css rel=stylesheet />
		<script type="text/javascript"  src="http://qzonestyle.gtimg.cn/qzone/app/qzlike/qzopensl.js#jsdate=20111201" charset="utf-8"></script>
		<script type="text/javascript">
			document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
	    	WeixinJSBridge.call('showToolbar');
			});	

			var novel_zh = '<?php echo getImageUrl("novel_zh.jpg");?>';
		</script>

	<title>假使明天不再来临</title>
</head>
<body style="width:95%;margin:auto;font-family:'microsoft yahei',Arial,宋体;font-size:62.5%;">
	<div style="font-size:1.4em;padding-bottom:2em">
	<script type="text/javascript">
		var novel_zh = '<?php echo getImageUrl("novel_zh.jpg");?>';
	</script>
	<div class="title">#故事<?php echo ($todayStory["release_date_title"]); ?>#</div>
	<div class="releaseAndShare" style="padding-bottom:1em;">
			<div class="release_date"><?php echo ($todayStory["release_date"]); ?>&nbsp;|&nbsp;评论(<?php echo ($todayStory["comment_counts"]); ?>)</div>
	</div>
	<div class="clear"></div>
	<div id="articleAndfunc">
		<div style="line-height:2em;border-top:0.1em solid lightgrey;border-bottom:0.1em solid lightgrey;padding-top:1em;padding-bottom:1em;"> 
			<?php echo ($todayStory["content"]); ?>
		</div>
		<div class="share_container" id="share_container">
			<div style="float:right;margin-right:1.5em;"><img src="__PUBLIC__/Images/renren.png" id="share_to_renren" class="share" /></div>
			<div style="float:right;margin-right:0.5em;"><img src="__PUBLIC__/Images/weibo.png" id="share_to_sina" class="share" /></div>
			<div style="float:right;margin-right:0.5em;"><img src="__PUBLIC__/Images/qq.png" id="share_to_qq" class="share" /></div>
		</div>
		<div class="clear"></div>
		<div style="text-align:center;margin-top:2em;color:grey;">
			<div style="float:left;width:25%;" id="love">
				<div style="height:3em;">
					<img src="__PUBLIC__/Images/dark_heart.png" style="width:2em;"/>
				</div>
				<div>喜欢</div>
			</div>
			<div style="float:left;width:25%;" id="hate">
				<div style="height:3em;">
					<img src="__PUBLIC__/Images/dark_ray.png" style="width:2em;" />
				</div>
				<div>雷人</div>
			</div>
			<div style="float:left;width:25%;" id="share_to">
				<div style="height:3em;">
					<img src="__PUBLIC__/Images/dark_share.png" style="width:2em;" />
				</div>
				<div>分享</div>
			</div>
			<div style="float:left;width:25%;" id="go_to_comment">
				<div style="height:3em;position:relative;">
					<?php if($todayStory['comment_counts'] > 9 and $todayStory['comment_counts'] < 100): ?><div style="position:absolute;top:0.3em;right:1.8em;color:white;"><?php echo ($todayStory["comment_counts"]); ?></div>
					<?php else: ?>
						<div style="position:absolute;top:0.3em;right:2em;color:white;"><?php echo ($todayStory["comment_counts"]); ?></div><?php endif; ?>
					<img src="__PUBLIC__/Images/comment_toatal.png" style="width:2.5em;"/>
				</div>
				
				<div>评论</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>

	<div id="articleAndComment">
		<div style="line-height:2em;border-top:0.1em solid lightgrey;border-bottom:0.1em solid lightgrey;padding-top:1em;padding-bottom:1em;"> 
			<?php echo ($todayStory["content_parts"]); ?>&nbsp;<span onclick="window.location.reload()">【查看原文】</span>
		</div>
		<div style="margin-top:1em;">
			<div>发表评论</div>
			<textarea class="comment_content" name="contents" id="contents">输入您的评论...</textarea>
			<span style="display:none;" id="warning">评论内容不能为空</span>
		</div>

		<div id="deliverycomment">
			<img src="__PUBLIC__/Images/tijiao_comment.png" />
		</div>
		<div id="commentlist">
			<?php if(is_array($commentlist)): foreach($commentlist as $key=>$v): ?><div class="user_comment">
				<div class="user_avatar">
					<?php if($v['follower_id'] == 0): ?><img src="__PUBLIC__/Images/user_avatar.png" />
					<?php else: ?><img src=<?php echo ($v["headimgurl"]); ?> /><?php endif; ?>
				</div>
				<div class="comment_info">
					<div class="username">
					<?php if(($v['follower_id'] == 0) or ($v['follower_name'] == 'anonymous')): ?>匿名用户
					<?php else: echo ($v["follower_name"]); endif; ?>
					</div>
					<div class="comment_time">
						<?php echo ($v["comment_time"]); ?>
					</div>
					<div class="clear"></div>
					<div class="contents">
						<?php echo ($v["contents"]); ?>
					</div>
					<!--<?php if(!empty($v['author_reply'])): ?><div class="reply"> 
						<em>&#9670;</em>
						<span>&#9670;</span>
						<div class="clear"></div>
						<div class="user_avatar">
							<img src="__PUBLIC__/Images/user_avatar.png" />
						</div>
						<div class="comment_info"> 
							<div class="username">
							作者
							</div>
							<div class="clear"></div>
							<div class="contents">
								<?php echo ($v["author_reply"]); ?>
							</div>
						</div>
					</div><?php endif; ?>-->
				</div>
					<?php if(!empty($v['author_reply'])): ?><div class="reply"> 
						<em>&#9670;</em>
						<span>&#9670;</span>
						<div class="user_avatar" style="padding-right:4%;padding-left:2%;">
							<img src="__PUBLIC__/Images/user_avatar.png" />
						</div>
						<div class="comment_info" style="width:74%;"> 
							<div class="username">
							作者回复
							</div>
							<div class="clear"></div>
							<div class="contents">
								<?php echo ($v["author_reply"]); ?>
							</div>
						</div>
					</div><?php endif; ?>
				<div class="clear"></div>
			</div><?php endforeach; endif; ?>
		</div>
		<div id="load_more" name="<?php echo ($next); ?>">点击加载更多...</div>
		<div id="tips">
			<img src="__PUBLIC__/Images/BouncingLoader.gif" />
			<span>正在努力加载...</span>
		</div>
		<div id="load_warning">加载失败</div>
	</div>
		 <script type="text/javascript">
			
			$(function(){
				var next = $("#load_more").attr("name");
				if (next != 0) {
					$("#load_more").css("display", "block");
				}

				$("#go_to_comment").click(function(){
					$("#articleAndComment").css("display", "block");
					$("#articleAndfunc").css("display", "none");
				});
				$("#share_to").click(function(){
					$("#share_to img").attr("src", "__PUBLIC__/Images/orange_share.png");
					$("#share_container").css("display", "block");
				});

				$("#share_to_qq").click(function(){
					$("#share_to_qq").attr("src", "__PUBLIC__/Images/qq.png");
					sharetoqq('','','','',novel_zh);
					$("#share_to_qq").attr("src", "__PUBLIC__/Images/light_qq.png");
				});

				$("#share_to_sina").click(function(){
					$("#share_to_sina").attr("src", "__PUBLIC__/Images/weibo.png");
					shareto('tsina','',novel_zh);
					$("#share_to_sina").attr("src", "__PUBLIC__/Images/light_weibo.png");
				});

				$("#share_to_renren").click(function(){
					$("#share_to_renren").attr("src", "__PUBLIC__/Images/renren.png");
					sharetorenren('','','',novel_zh);
					$("#share_to_renren").attr("src", "__PUBLIC__/Images/light_renren.png");
				});


			/*	$("#commentbtn").click(function(){
					$("html, body").animate({scrollTop: $(document).height()}, 'slow');
				});*/

			
				$('#contents').focus(function(){
					$("#contents").val("");
					$("#contents").css("color", "black");
				});

				$("#load_more").click(function(){
					var  handleUrl = '<?php echo U("Frontend/Index/loadcomment");?>';
					var novelId = <?php echo ($todayStory["id"]); ?>;
					var loadmore = $("#load_more");
					var next = loadmore.attr("name");
					loadmore.css("display", "none");
					var tips = $("#tips");
					tips.css("display", "block");
					$.post(
						handleUrl,
						{
							'p':next,
							'novel_id':novelId
						},
						function(data)
						{
							if (data.status == 1) 
							{
								var list = data.list;
								tips.css("display", "none");
								$("#commentlist").append(list);
								loadmore.attr("name",data.next);
								if (data.next != 0 )
								{ 
									loadmore.css("display", "block");
								}
							}
							else if (data.status == 2)
							{
								$("#load_warning").css("display", "block");
							}
						}
					)
				});

				$("#love").one("click",function(){
					$("#love img").attr("src", "__PUBLIC__/Images/orange_heart.png");
					var handleUrl = '<?php echo U("Frontend/Index/handlelike");?>';
					var novelId = <?php echo ($todayStory["id"]); ?>;
					$.post(
						handleUrl,
						{
							'novel_id':novelId
						},
						function(data)
						{
						},
						'json'
					);
				});
				$("#hate").one("click", function(){
					$("#hate img").attr("src", "__PUBLIC__/Images/orange_ray.png");
					var handleUrl = '<?php echo U("Frontend/Index/handleray");?>';
					var novelId = <?php echo ($todayStory["id"]); ?>;
					$.post(
						handleUrl,
						{
							'novel_id':novelId
						},
						function(data)
						{
						},
						'json'
					);
				});
				$('#deliverycomment').click(function(){
				//	$("#deliverycomment img").attr("src", '__PUBLIC__/Images/dark_comment.png');
					var contents = $('textarea[name=contents]');
					if ($.trim(contents.val()) == '') {
						$("#warning").css("display", "block");
						contents.focus();
						return;
					} else {
						$("#warning").css("display", "none");
					}
					
					var novelId = <?php echo ($todayStory["id"]); ?>;
					var handleUrl = '<?php echo U("Frontend/Index/handlecomment");?>';
					$.post(
						handleUrl,
						{
							'contents':contents.val(),
							'novel_id':novelId
						},
						function(data)
						{
							if (data.status == 1) {
								var str = '<div class="user_comment">' + 
											'<div class="user_avatar">'+
												'<img src="__PUBLIC__/Images/user_avatar.png" />'+
											'</div>'+
											'<div class="comment_info">'+
										  		'<div class="username">匿名用户</div>'+
										  		'<div class="comment_time">' + data.comment_time + '</div>'+
												'<div class="clear"></div>'+
												'<div class="contents">' + data.contents + '</div>'+
										   '</div>'+
										   '<div class="clear"></div>'+
										  '</div>';
								$("#commentlist").prepend(str);
								$("#contents").val("输入您的评论...");
							} else if(data.status == 2){
								$("#warning").text("评论内容不能为空");
								$("#warning").css("display","block");
							} else if (data.status == 3) {
								$("#warning").text("评论失败，请重新输入评论内容");
								$("#warning").css("display", "block");
							}
							$("#deliverycomment img").attr("src", '__PUBLIC__/Images/tijiao_comment.png');
						},
						'json'
					);
						
				});
			});
 		</script>
</div>


</body>
</html>