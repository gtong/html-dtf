<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
		<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1" />
		<script type="text/javascript" src="__PUBLIC__/Js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="__PUBLIC__/Js/shareto.js"></script>	
		<link href="__PUBLIC__/Css/zhanghui/frontend.css" type=text/css rel=stylesheet />
		<script type="text/javascript"  src="http://qzonestyle.gtimg.cn/qzone/app/qzlike/qzopensl.js#jsdate=20111201" charset="utf-8"></script>
		<script type="text/javascript">
			document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
	    	WeixinJSBridge.call('showToolbar');
			});	

			var novel_zh = '<?php echo getImageUrl("novel_zh.jpg");?>';
		</script>

	<title>假使明天不再来临</title>
</head>
<body style="width:95%;margin:auto;font-family:'microsoft yahei',Arial,宋体;font-size:62.5%;">
	<div class="future_banner">
	<img src="__PUBLIC__/Images/banner.png" />
</div>
<div style="font-size:1.5em;margin-top:1.5em;" class="line_height">
写作是一件寂寞的事情，需要支持和鼓励，虽然讲故事的人并不以此为生，还是希望得到来自读者的肯定，如果在看完了已有的故事之后，还希望继续看下去，请点击“我要预定”，就可以在未来的每一天看到作者继续写下去的所有故事，1次点击犹如1票，给作者一点信心，一点鼓励，一点快乐，一点坚持下去的勇气。
谢谢
</div>
<div id="booknovel">
	<img src="__PUBLIC__/Images/light_novelbook.png" />
<div>

<div id="novelbook_warning" style="text-align:cener;width:100%;display:none;">
</div>

<script type="text/javascript">
	$(function(){
		$('#booknovel').click(function(){
			$('#booknovel img').attr("src", "__PUBLIC__/Images/dark_novelbook.png");
			var handleUrl = '<?php echo U("Frontend/Index/handlebookstory");?>';
			$.post(
				handleUrl,
				{
				},
				function(data)
				{
					if (data.status == 1) {
						window.location = '<?php echo U("Frontend/Index/futurelist");?>';
					} else if(data.status == 0) {
						$("#novelbook_warning").css("display", "block");
						$("#novelbook_warning").text("预定失败");
					} else if(data.status == 2) {
						$("#novelbook_warning").css("display", "block");
						$("#novelbook_warning").text("您没有关注此服务号,不能预定");
					}
					$('#booknovel img').attr("src", "__PUBLIC__/Images/light_novelbook.png");

				},
				'json'
			);
				
		});
	});
</script>

</body>
</html>