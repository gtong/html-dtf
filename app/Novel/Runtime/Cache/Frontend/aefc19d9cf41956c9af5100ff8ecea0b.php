<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head>
		<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1" />
		<script type="text/javascript" src="__PUBLIC__/Js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="__PUBLIC__/Js/shareto.js"></script>	
		<script type="text/javascript"  src="http://qzonestyle.gtimg.cn/qzone/app/qzlike/qzopensl.js#jsdate=20111201" charset="utf-8"></script>
		<script type="text/javascript">
			document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
	    	WeixinJSBridge.call('showToolbar');
			});	

			var novel_zh = '<?php echo getImageUrl("novel_zh.jpg");?>';
		</script>

	<title>假使明天不再来临</title>
</head>
<body style="width:90%;margin:auto;font-size:62.5%;">
	<div style="text-align:center;">
	<div id="novellist">
	<?php if(is_array($list)): foreach($list as $key=>$v): ?><div style="border-bottom:0.1em solid grey;height:6em;line-height:6em;">
			<a href="__APP__/Frontend/Index/comment/id/<?php echo ($v["id"]); ?>" style="float:left;width:65%;text-align:left;text-decoration:none;color:black;font-size:2.5em;text-align:left;font-weight:bold;"><?php echo ($v["title"]); ?><a/>
			<span style="font-size:1.5em;width:35%;float:left;text-align:right;"><?php echo ($v["release_date"]); ?></span>
		</div><?php endforeach; endif; ?>
	</div>
	<!--<div style="position:fixed;bottom:0px;"><?php echo ($page); ?></div> -->
	<div style="clear:both"></div>

	<div style="margin-top:2em;color:white;">
		<div name="<?php echo ($prev); ?>" id="prev"  style="font-size:3em;width:42%;text-align:center;cursor:pointer;background:url('__PUBLIC__/Images/comment.png') 0 0 no-repeat;float:left;">
			上一页	
		</div>

		<div id="next" name="<?php echo ($next); ?>" style="font-size:3em;width:42%;text-align:center;cursor:pointer;background:url('__PUBLIC__/Images/comment.png') 0 0 no-repeat;float:right;">
		下一页
		</div>
	</div>
</div>

		 <script type="text/javascript">
					
			$(function(){
					var handleUrl = '<?php echo U("Frontend/Index/page/");?>';
							$('#prev').click(function(){
								var prev = $("#prev").attr("name");
								if (prev == 0) {
									return ;
								}
									$.post(
										handleUrl,
										{
											'p':prev,
											'status':'future'
										},
										function(data)
										{
											var list = data.list;
											var prev = data.prev;
											var next = data.next;
											$("#prev").attr("name", prev);
											$("#next").attr("name", next);
											$("#novellist").html(list);
										},
										'json'
									);
							});
							$('#next').click(function(){
								var next = $("#next").attr("name");
								if (next == 0 ){
									return;
								}
									$.post(
										handleUrl,
										{
											'p':next,
											'status':'future'
										},
										function(data)
										{
											var list = data.list;
											var prev = data.prev;
											var next = data.next;
											$("#prev").attr("name", prev);
											$("#next").attr("name", next);
											$("#novellist").html(list);
										},
										'json'
									);
							});


			});
		</script>

</body>
</html>