<?php
class IndexAction extends CommonAction{
	public function index() {
		$this->data = array();
		$this->display();
	}
	
	public function logout() {
		session_unset();
		session_destroy();
		cookie("LOGIN", null);
		$this->redirect('Admin/Login/index');
	}
	
	public function noveladd() {
		if (!IS_POST) {
			halt("页面不存在");
		}
		
		$release_date = I('release_date');
		if (!$release_date) {
			$this->error('日期不能为空');
		}
		
		$title = I('title');
	/*	if (!$title) {
			$this->error('标题不能为空');
		}*/

		$content = I('content');
		if (!$content) {
			$this->error("内容不能为空");
		}
		$synopsis = I("synopsis");
		
		$data = array(
				"content" => $content,
				"synopsis" => $synopsis,
				'update_time' => time(),
				'release_date' => $release_date,
				'title' => $title
				);
		
		$novel = M('novellist');
		$novel_id = I('novel_id');

		if ($novel_id > 0) {
			if (!$novel->where("id=$novel_id")->save($data)) {
				$this->error("更新失败");
			}
		} else {
			if (!$novel->add($data)) {
				$this->error("存入数据库错误");
			}
		}
		$this->redirect("Admin/Index/novellist");	
	}
	
	
	public function novellist() {
		$novel = M('novellist');
		$novellist = $novel->join("left join weixin_novel_degree on weixin_novellist.id = weixin_novel_degree.novel_id")
						   ->field("weixin_novellist.*,weixin_novel_degree.like_count, weixin_novel_degree.ray_count")->select();
		$this->counts = $novel->count();
		$this->novellist = $novellist;
		$this->display();
	}
	
	public function edit() {
		$id = I('id');
		$novel = M('novellist');
		$data = $novel->find($id);
		//$data['release_date'] = date("Y-m-d", $data['release_date']);
		$this->data = $data;
		$this->display('index');
	}
	
	public function reply() {
		$id = I('id');
		$novelcomment = M('novelcomment');
		$data = $novelcomment->field('contents,author_reply')->find($id);
		$comment = $data['contents'];
		$reply = $data['author_reply'];
		$this->id = $id;
		$this->comment = $comment;
		$this->reply = $reply;
		$this->display();
	}
	public function replysave() {
		if (!IS_POST) {
			halt("页面不存在");
		}
		
		$reply = I('reply');
		$id = I('comment_id');
		
		$data = array(
				"id" => $id,
				"author_reply" => $reply
				);
		
		$novel = M('novelcomment');
		$datas = $novel->field('novel_id')->find($id);
		$novel_id = $datas['novel_id'];

		if (!$novel->save($data)) {
			$this->error("更新失败");
		}
		$this->redirect("Admin/Index/comment", array("id"=>$novel_id));	
	}

	public function comment() {
		$id = I('id');
		$comment = M('novelcomment');
		/*$data = $comment->join("inner join weixin_followers ON weixin_novelcomment.novel_id = $id AND weixin_novelcomment.follower_id = weixin_followers.id")
						->field("weixin_followers.follower_name,weixin_novelcomment.*")
						->select();*/

		$data = $comment->where(array("novel_id"=>$id))->select();
		$novel = M('novellist');
		$release_date = $novel->field("release_date")->find($id);
		$this->release_date = $release_date['release_date'];
		$this->comments = $data;
		$this->comment_counts = $comment->where("novel_id=$id")->count();
		$this->display();
	}
	
	public function delcomment() {
		$id = I('id');
		$comment = M('novelcomment');
		$novellist= M('novellist');
		$comment->startTrans();
		$novel_id = $comment->where("id=$id")->field('novel_id')->find();
		$novel_id = $novel_id['novel_id'];
		$delRows = $comment->where("id=$id")->delete();
		$updateRows = $novellist->where("id=$novel_id")->setDec("comment_counts", 1);	
		if ($delRows && $updateRows) {
			$comment->commit();
		}else {
			$comment->rollback();
		}
		
		$this->redirect("Admin/Index/comment", array("id"=>$novel_id));
	}
	
	public function book() {
		$novelbook = M("novelbook");
		$data = $novelbook->join("inner join weixin_followers ON weixin_novelbook.follower_id = weixin_followers.open_id")
						  ->field("weixin_followers.follower_name, weixin_novelbook.*")
						  ->select();
		$this->counts = $novelbook->count();
		$this->data = $data;
		$this->display();
	}
}
