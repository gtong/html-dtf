<?php
class LoginAction extends Action {
	public function index() {
		$this->display('temp');
	}
	
	public function login() {
		if (!IS_POST) {
			halt("页面不存在");
		}
		$verify = I('code', '', 'md5');
		
		if ($verify != session('verify')) {
			$this->error('验证码不正确');
		}
		
		$username = I('username');
		$pwd = I('password', '', 'md5');
		
		$user = M('user')->where(array('username' => $username))->find();
		if (!user || $user['password'] != $pwd) {
			$this->error('账号或者密码错误');
		}
		
		if ($user['lock']) {
			$this->error('用户被锁住');
		}
		
		$data = array(
				'id' => $user['id'],
				'logintime' => time(),
				'loginip' => get_client_ip()
				);
		M('user')->save($data);
		
		//记录session
		session('uid', $user['id']);
		session('username', $user['username']);
		session('logintime', date("Y-m-d H:i:s", $user['logintime']));
		cookie("LOGIN", 1);
		$this->redirect("Admin/Index/index");
	}
	
	public function verify() {
		import('ORG.Util.Image');
		Image::buildImageVerify();
	}
}
