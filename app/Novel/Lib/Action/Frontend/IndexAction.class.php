<?php
define("COMMENT_PAGE_COUNT", 10);
class IndexAction extends CommonAction {
    public function index(){
		$this->display();
    }


	public function bookstory() {
		$this->display();
	}
	
	public function handlebookstory() {
		if (!IS_AJAX) {
			halt("页面不存在");
		}
		$open_id = $_SESSION["open_id"];
		if (empty($open_id)) {
			halt("页面不存在");
		}
		$bookstory = M("novelbook");
		$follower = M("followers");
		$condition["follower_id"] = "$open_id"; 
		$cond["open_id"] = "$open_id";
		$followerinfo = $follower->where($cond)->find();
		if (empty($followerinfo)) {
			$this->ajaxReturn(array("status"=>2), 'json');
		}
		$bookinfo = $bookstory->where($condition)->find();
		if (empty($bookinfo)) {
			$bookstory->follower_id = "$open_id";
			
			if (!$bookstory->add()) {
				$this->ajaxReturn(array("status"=>0), 'json');
			}
		}

		$this->ajaxReturn(array("status" => 1), 'json');
		//$this->redirect("Frontend/Index/futurelist");

	}
	
	public function futurelist() {
		$novellist = M("novellist");
		$date = date("Y-m-d");
		$map["release_date"] = array("gt", $date);
		$count = $novellist->where($map)->count();
		$list = $novellist->where($map)->page('1,5')->select();
		$totalPages = ceil($count/5);
		$this->list = $list;
		$this->prev = 0;
		$this->next = $totalPages == 1 ? 0 : 2;
		$this->status = "future";
		$this->display("Previous:index");

	}
	
	public function page() {
		$nowpage = isset($_POST['p']) ? $_POST['p'] : 1;
		$date = date("Y-m-d");
		$novellist = M("novellist");
		//if ($_POST['status'] == "previous") {
			$map["release_date"] = array("lt", $date);
	//	} else {
		//	$map["release_date"] = array("gt", $date);
	//	}
		$count = $novellist->where($map)->count();
		$totalPages = ceil($count/5);
		$prev = $nowpage-1;
		$next = ($nowpage + 1)>$totalPages ? 0 : $nowpage+1;
		$list = $novellist->where($map)->order("release_date desc")->page($nowpage. ',5')->select();

		$str = "";
		foreach ($list as $listInfo) {
			$id = $listInfo["id"];
			$title = $listInfo["title"];
			$release_date = $listInfo["release_date"];
			$release_date = date("Ymd", strtotime($release_date));
			$comment_counts = $listInfo["comment_counts"];
			$synopsis = $listInfo["synopsis"];
			$comment_img = getImageUrl('comment_img.png');
			$read_more_img = getImageUrl('read_more_img.png');
			$str .= '<a href=' . __APP__ . '/Frontend/Index/comment/id/'. $id . ' class="listinfo">'.
						'<div class="synopsis">'.$synopsis.'<br />#故事'.$release_date.'#</div>'.
						'<div class="line"></div>'.
						'<div class="past_read_more">'.
							'<div class="left_div">阅读全文</div>'.
							'<div class="right_div">
								<img src=' . $read_more_img . ' style="width:0.5em;"/>
							</div>'.
						'</div>'.
						'<div style="clear:both"></div>'.
						
					'</a>';
		}
		$data['prev'] = $prev;
		$data['next'] = $next;
		$data['list'] = $str;
		$this->ajaxReturn($data, 'json');
	}
	
	public function loadcomment() {
		$nowpage = $_POST['p'];
		$novel_id = $_POST['novel_id'];
		if (empty($nowpage) || empty($novel_id)) {
			$this->ajaxReturn(array("status"=>2), 'json');
		}
		$novelcomment = M("novelcomment");
		$count = $novelcomment->where("novel_id=$novel_id")->count();
		$totalpage = ceil($count/COMMENT_PAGE_COUNT);
		$next = ($nowpage+1)>$totalpage ? 0 : $nowpage + 1;
		//$commentlist = $novelcomment->where("novel_id=$novel_id")->order("comment_time desc")->page($nowpage . ',' . COMMENT_PAGE_COUNT)->select();
		$commentlist = $novelcomment->join("weixin_followers on weixin_novelcomment.follower_id=weixin_followers.id")
						->where(array("weixin_novelcomment.novel_id"=>$novel_id))->order("comment_time desc")->field('weixin_novelcomment.*, weixin_followers.follower_name, weixin_followers.headimgurl')
						->page($nowpage.','.COMMENT_PAGE_COUNT)->select();
		$list = "";
		$avatar = getImageUrl('user_avatar.png');
		foreach ($commentlist as $info) {
				if ($info['follower_id'] == 0 || $info['follower_name'] == 'anonymous'){
					$follower_name = '匿名用户';
				} else {
					$follower_name = $info['follower_name'];
				}

				if ($info['follower_id'] == 0) {
					$user_avatar = "/app/Public/Images/user_avatar.png";
				} else {
					$user_avatar = $info['headimgurl'];
				}
				$list .= '<div class="user_comment">'.
							'<div class="user_avatar">'.
								'<img src=' . $user_avatar . ' />'.
							'</div>'.
							'<div class="comment_info">'.
								'<div class="username">' . $follower_name . '</div>'.
								'<div class="comment_time">'.
									$info["comment_time"].
								'</div>'.
								'<div class="clear"></div>'.
								'<div class="contents">'.
									$info["contents"].
								'</div>'.
							'</div>'.
							'<div class="clear"></div>'.
						'</div>';
		}
		$data["status"] = 1;
		$data["list"] = $list;
		$data["next"] = $next;
		$this->ajaxReturn($data, 'json');
	}
	public function comment() {
		$novellist = M("novellist");
		if (I("id") != "") {
			$novel_id = I("id");
			$todayStory = $novellist->find($novel_id);
			$todayStory['content_parts'] = nl2br(mb_substr($todayStory['content'], 0, 150, 'UTF-8'))."......";
			$todayStory['content'] = nl2br($todayStory['content']);
			$todayStory['release_date_title'] = date("Ymd", strtotime($todayStory["release_date"]));
		} else {
			$date = date("Y-m-d");
			$condition["release_date"] = $date;
			$todayStory = $novellist->where($condition)->find();
			$todayStory['content'] = nl2br($todayStory['content']);
			$todayStory['content_parts'] = mb_substr($todayStory['content'], 0, 150, 'UTF-8')."......";
			$todayStory['release_date_title'] = date("Ymd", strtotime($todayStory["release_date"]));
			$novel_id = $todayStory["id"];	
		}
		$novelcomment = M("novelcomment");
		//$commentlist = $novelcomment->where("novel_id=$novel_id")->order("comment_time desc")->page('1,'.COMMENT_PAGE_COUNT)->select();
		$commentlist = $novelcomment->join("weixin_followers on weixin_novelcomment.follower_id=weixin_followers.id")
						->where(array("weixin_novelcomment.novel_id"=>$novel_id))->order("comment_time desc")->field('weixin_novelcomment.*, weixin_followers.follower_name, weixin_followers.headimgurl')
						->page('1,' . COMMENT_PAGE_COUNT)->select();
		
		$count = $novelcomment->where("novel_id=$novel_id")->count();
		$totalPages = ceil($count/COMMENT_PAGE_COUNT);
		$next = $totalPages == 1 ? 0 : 2;
		if ($count == 0) {
			$next = 0;
		}
		$this->commentlist = $commentlist;
		$this->next = $next;
		$this->todayStory = $todayStory;
		$this->display();
	}
	
	public function handlecomment() {
		if (!IS_AJAX) {
			halt("页面不存在");
		}

		$contents = I("contents");
		if (empty($contents)) {
			$this->ajaxReturn(array("status"=>2), 'json');
		}
		$novel_id = I("novel_id");
		//$follower_id = $_SESSION['open_id'];

		$novelcomment = M("novelcomment");
		$novelcomment->contents = $contents;
		$novelcomment->novel_id = $novel_id;
		//$condition["open_id"] = "$follower_id";
		//$follower_id = M("followers")->where($condition)->field("id")->find();

		//$novelcomment->follower_id = $follower_id['id'];
		$novelcomment->follower_id = 0;
		if (!$novelcomment->add()) {
			$this->ajaxReturn(array("status"=>3), 'json');
		}
		$novellist = M("novellist");
		$updateRows = $novellist->where("id=$novel_id")->setInc("comment_counts", 1);	

		$data['contents'] = $contents;
		$data['comment_time'] = date("Y-m-d H:i:s");
		$data['status'] = 1; 
		$this->ajaxReturn($data, 'json');
		 
	}

	function handlelike() {
		$novel_degree = M('novel_degree');
		$novel_id = I("novel_id");
		$data = $novel_degree->where(array("novel_id"=>$novel_id))->find();
		if (empty($data)) {
			$novel_degree->like_count = 1;
			$novel_degree->novel_id = $novel_id;
			$novel_degree->add();
		} else {
			$novel_degree->where(array("novel_id"=>$novel_id))->setInc("like_count", 1);
		}
		$this->ajaxReturn(array("status"=>1), 'json');
	}

	function handleray() {
		$novel_degree = M('novel_degree');
		$novel_id = I('novel_id');
		$data = $novel_degree->where(array("novel_id"=>$novel_id))->find();
		if (empty($data)) {
			$novel_degree->ray_count = 1;
			$novel_degree->novel_id = $novel_id;
			$novel_degree->add();
		} else {
			$novel_degree->where(array("novel_id"=>$novel_id))->setInc("ray_count", 1);
		}
		$this->ajaxReturn(array("status"=>1), 'json');
	}

	public function about() {
		$this->display();
	}
}
