<?php
class PreviousAction extends CommonAction{
	public function index(){
		//import('ORG.Util.Page');	
		$date = date("Y-m-d");
		$novellist = M("novellist");
		$map["release_date"] = array("lt", $date);
		$count = $novellist->where($map)->count();
		if ($count == 0) {
			return;
		}
		//$page = new Page($count, 5);
	//	$page->setConfig("header","");
	//	$page->setConfig("theme", ' %upPage% %downPage%');
		//$nowpage = isset($_GET['p']) ? $_GET['p'] : 1;
		$list = $novellist->where($map)->order("release_date desc")->page('1, 5')->select();
	//	$show = $page->show();
	//	$this->page = $show;
		$totalPages = ceil($count/5);
		$this->list = $list;
		$this->prev = 0;
		$this->next = $totalPages == 1 ? 0 : 2;
		//$this->status = "previous";
		$this->display();
	}
	
	public function page() {
		$nowpage = isset($_POST['p']) ? $_POST['p'] : 1;
		$date = date("Y-m-d");
		$novellist = M("novellist");
		$map["release_date"] = array("lt", $date);
		$count = $novellist->where($map)->count();
		$totalPages = ceil($count/5);
		$prev = $nowpage-1;
		$next = ($nowpage + 1)>$totalPages ? 0 : $nowpage+1;
		$list = $novellist->where($map)->order("release_date desc")->page($nowpage. ',5')->select();

		$str = "";
		foreach ($list as $listInfo) {
			$id = $listInfo["id"];
			$title = $listInfo["title"];
			$release_date = $listInfo["release_date"];
			$str .= '<div style="padding-top:1.5em;padding-bottom:1.5em;">'.
						'<a href=' . __APP__ . '/Frontend/Index/comment/id/'. $id . ' style="text-decoration:none;color:black;font-size:2.5em;text-align:left;font-weight:bold;">'.
							$title.
						'</a>'.
						'<span style="font-size:1.5em;">'.
							$release_date.
						'</span>'.
					'</div>'.
					'<hr />';
		}
		$data['prev'] = $prev;
		$data['next'] = $next;
		$data['list'] = $str;
		$this->ajaxReturn($data, 'json');
	}
}
?>
