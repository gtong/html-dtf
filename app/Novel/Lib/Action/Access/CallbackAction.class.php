<?php
//$wechatObj->responseMsg();

class CallbackAction extends Action {

	const TOKEN = "yamaloo520";
	public function index() {
		//$this->valid();
		$this->responseMsg();
	}

	public function valid(){
		$echoStr = $_GET["echostr"];

		//valid signature , option
		if($this->checkSignature()){
			echo $echoStr;
			exit;
		}
    }

    public function responseMsg(){
		//get post data, May be due to the different environments
		$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];

      	//extract post data
		if (!empty($postStr)){
				
				$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
				$fromUsername = $postObj->FromUserName;
				$toUsername = $postObj->ToUserName;
				$keyword = trim($postObj->Content);

				$MsgType = $postObj->MsgType;
				$Event = $postObj->Event;
				if ($MsgType == "event") {
					if ($Event == "subscribe") {
							$follower = M("followers");
							$condition['open_id'] = "$fromUsername";
							if ($follower->where($condition)->count()==0) {
								$follower->open_id = "$fromUsername";
								$follower->add();
							} else {
								$follower->where($condition)->setField("subscribed",1);
							}
							
							$time = time();
							$textTpl = "<xml>
										<ToUserName><![CDATA[%s]]></ToUserName>
										<FromUserName><![CDATA[%s]]></FromUserName>
										<CreateTime>%s</CreateTime>
										<MsgType><![CDATA[%s]]></MsgType>
										<ArticleCount>%s</ArticleCount>
										<Articles>
										<item>
										<Title><![CDATA[%s]]></Title>
										<Description><![CDATA[%s]]></Description>
										<Url><![CDATA[%s]]></Url>
										</item>
										</Articles>
										<FuncFlag>0</FuncFlag>
										</xml>";             
							$msgType = "news";
							$articleCounts = 1;

							$novellist = M("novellist");
							$date = date("Y-m-d");
							$condition["release_date"] = $date;
							$title = "假使明天不再来临，今晚给你讲个故事好吗？";
							$desc = $novellist->where($condition)->getField("synopsis");
							$date = date("Ymd");
							$desc .= "\n\n#故事$date#";
							$todayUrl = U("Frontend/Index/comment", array("uid" => $fromUsername),true, false, true);
							$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $articleCounts, $title, $desc, $todayUrl);
							echo $resultStr;
					} elseif($Event == "unsubscribe") {
							$follower = M("followers");
							$condition['open_id'] = "$fromUsername";
							$follower->where($condition)->setField("subscribed",0);
					} elseif($Event == "CLICK" && $postObj->EventKey == "HOME_INFO") {
							$time = time();
							$textTpl = "<xml>
										<ToUserName><![CDATA[%s]]></ToUserName>
										<FromUserName><![CDATA[%s]]></FromUserName>
										<CreateTime>%s</CreateTime>
										<MsgType><![CDATA[%s]]></MsgType>
										<ArticleCount>%s</ArticleCount>
										<Articles>
										<item>
										<Title><![CDATA[%s]]></Title>
										<Description><![CDATA[%s]]></Description>
										<Url><![CDATA[%s]]></Url>
										</item>
										</Articles>
										<FuncFlag>0</FuncFlag>
										</xml>";             
							$msgType = "news";
							$articleCounts = 1;

							$novellist = M("novellist");
							$date = date("Y-m-d");
							$condition["release_date"] = $date;
							$title = "假使明天不再来临，今晚给你讲个故事好吗？";
							$desc = $novellist->where($condition)->getField("synopsis");
							$date = date("Ymd");
							$desc .=  "\n\n#故事$date#";
							//$picUrl = 'http://42.96.174.106/app/Public/Images/update_banner.png';
							$todayUrl = U("Frontend/Index/comment", array("uid" => $fromUsername),true, false, true);

							/*$pastStory = '温习过去每一天的故事';
							$pastPicUrl = 'http://42.96.174.106/app/Public/Images/update_history.png';
							$pastUrl = U("Frontend/Previous/index",array("uid => $fromUsername"), true, false, true);
							$novelbook = M("novelbook");
							$condition["follower_id"] = "$fromUsername";
							$data = $novelbook->where($condition)->find();
							$futureStory = '预定未来每一天的故事';
							$futurePicUrl = 'http://42.96.174.106/app/Public/Images/update_future.png';
							$futureUrl = U("Frontend/Index/bookstory", array("uid" => $fromUsername), true, false, true);
							if (!empty($data)) {
								$futureStory = '未来每一天的故事';
								$futureUrl = U("Frontend/Index/futurelist", array("uid" => $fromUsername), true, false, true);
							}*/
							$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $articleCounts, $title, $desc,$todayUrl);
							echo $resultStr;
				   }
							
			}

        }else {
        	echo "";
        	exit;
        }
    }

	private function checkSignature(){
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];	
        		
		$token = self::TOKEN;
		$tmpArr = array($token, $timestamp, $nonce);
		sort($tmpArr);
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );
		
		if( $tmpStr == $signature ){
			return true;
		}else{
			return false;
		}
	}
}
?>
