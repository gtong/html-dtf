function shareto(m,title,pic,url)
{
	if (!title) title = encodeURIComponent(document.title);
	if (!url) { 
		url = encodeURIComponent(document.location.href);
	} else {
		url = encodeURIComponent(url);
	}
	if (!pic) pic = '';
	
	if (m == "qq") {
		window.open('http://shuqian.qq.com/post?from=3&title='
								+ title + '&uri=' + url								
								+ '&jumpback=2&noui=1',
						'favit',
						'width=930,height=470,toolbar=no,menubar=no,location=no,scrollbars=yes,status=yes,resizable=yes,left='
						+ (screen.width - 930) / 2 + ',top='
						+ (screen.height - 470) / 2);
	} else if (m == "tsina") {
		var searchPic = false;
		void ((function(s, d, e) {
			var f = 'http://v.t.sina.com.cn/share/share.php?', p = [
					'appkey=2505924389', '&url=', url,   '&title=', title, '&pic=', pic, '&searchPic=', searchPic]
					.join('');
			function a() {
				if (!window.open(
								[ f, p ].join(''),
								'mb',
								['toolbar=0,status=0,resizable=1,width=620,height=450,left=',
										(s.width - 620) / 2, ',top=',
										(s.height - 450) / 2 ].join('')))
					document.location.href = [ f, p ].join('');
			}
			;
			if (/Firefox/.test(navigator.userAgent)) {
				setTimeout(a, 0)
			} else {
				a()
			}
		})(screen, document, encodeURIComponent));
	} else if (m == "douban") {
		void (function() {
			var d = document, e = encodeURIComponent, s1 = window.getSelection, s2 = d.getSelection, s3 = d.selection, s = s1 ? s1()
					: s2 ? s2() : s3 ? s3.createRange().text : '', r = 'http://www.douban.com/recommend/?url='
					+ url
					+ '&title='
					+ title
					+ '&sel='
					+ e(s) + '&v=1', x = function() {
				if (!window.open(r, 'douban','toolbar=0,resizable=1,scrollbars=yes,status=1,width=450,height=355,left='
									+ (screen.width - 450) / 2 + ',top='
									+ (screen.height - 330) / 2))
					document.location.href = r + '&r=1'
			};
			if (/Firefox/.test(navigator.userAgent)) {
				setTimeout(x, 0)
			} else {
				x()
			}
		})();
	} else if (m == "renren") {
		// 官方分享方式1
//		var connect_url = "http://www.connect.renren.com";
//		var url = window.location.href;
//		var addQS = function(url, qs) {
//			var a = [];
//			for ( var k in qs)
//				if (qs[k])
//					a.push(k.toString() + '=' + encodeURIComponent(qs[k]));
//			return url + '?' + a.join('&');
//		}
//		var href = addQS(connect_url + '/sharer.do', {
//			'url' : url,
//			'title' : url == window.location.href ? document.title : null
//		});
//		window.open(href, 'sharer',
//				'toolbar=0,status=0,width=550,height=400,left='
//						+ (screen.width - 550) / 2 + ',top='
//						+ (screen.height - 500) / 2);

		// 官方分享方式2
		 void ((function(s, d, e) {
		 if (/renren\.com/.test(d.location))
		 return;
		 var f = 'http://share.renren.com/share/buttonshare.do?link=', u =
		 d.location, l = d.title, p = [
		 e(u), '&title=', e(l) ].join('');
		 function a() {
		 if (!window.open(
		 [ f, p ].join(''),
		 'xnshare',
		 [
		 'toolbar=0,status=0,resizable=1,width=626,height=436,left=',
		 (s.width - 626) / 2, ',top=',
		 (s.height - 436) / 2 ].join('')))
		 u.href = [ f, p ].join('');
		 };
		 if (/Firefox/.test(navigator.userAgent))
		 setTimeout(a, 0);
		 else
		 a();
		 })(screen, document, encodeURIComponent));
	}else if (m == "douban") {
		void (function() {
			var d = document, e = encodeURIComponent, s1 = window.getSelection, s2 = d.getSelection, s3 = d.selection, s = s1 ? s1()
					: s2 ? s2() : s3 ? s3.createRange().text : '', r = 'http://www.douban.com/recommend/?url='
					+ e(d.location.href)
					+ '&title='
					+ e(d.title)
					+ '&sel='
					+ e(s) + '&v=1&n=1', x = function() {
				if (!window
						.open(r, 'douban9',
								'toolbar=0,resizable=1,scrollbars=yes,status=1,width=450,height=330'))
					location.href = r + '&r=1'
			};
			if (/Firefox/.test(navigator.userAgent)) {
				setTimeout(x, 0)
			} else {
				x()
			}
		})();
	}else if (m == "qzone") {// 未开放分享
		window.open("http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url="
				+ url + '&title=' + title + '&desc=' + title + '&summary=' + title, 
				'qzone',
		'toolbar=0,status=0,width=900,height=760,left='
				+ (screen.width - 900) / 2 + ',top='
				+ (screen.height - 760) / 2);
	} 
}

/*
@desc 分享内容至QQ空间
@params:
{
2	title:		分享标题(最长36个汉字)
2	url:		点击分享标题跳转页面(http://)
2	desc:		点击分享标题跳转页面(http://)
3	summary:	分享摘要(最长80个汉字)
4	pics:		分享资源附带图片资源（最长255个字符, http://开头, 多图以'|'分割, 最佳100*100）
6	site:		分享的来源说明
}
*/
function sharetoqq(title, url, desc, summary, pics, site)
{
	if (!title)	title = document.title;
	if (!url)	url = document.location.href;
	site = '假使明天不再来临';
	var params = "?title=" + encodeURIComponent(title) + '&url=' + encodeURIComponent(url) + '&site=' + encodeURIComponent(site);	
	if (desc) params += '&desc=' + encodeURIComponent(desc);
	if (summary) params += '&summary=' + encodeURIComponent(summary);
	if (pics) params += '&pics=' + encodeURIComponent(pics);
	
	var baseurl = "http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey";	
	window.open(baseurl + params, 'qzone', 'toolbar=0,status=0,width=900,height=760,left=' + (screen.width-900)/2 + ',top=' + (screen.height-760)/2);
}

/*
 * resourceUrl:	分享的资源Url
 * srcUrl:		分享的资源来源Url,默认为header中的Referer,如果分享失败可以调整此值为resourceUrl试试
 * pic:			分享的主题图片Url
 * title:		分享的标题
 * description:	分享的详细描述
 */
function sharetorenren(title, resourceUrl, description, pic)
{
//	resourceUrl = 'http://www.yamaloo.cn';
	var isIE = navigator.userAgent.match(/(msie) ([\w.]+)/i);
	function a() {
		if (!title)	title = document.title;
		if (!resourceUrl)	resourceUrl = document.location.href;
		p = {};
		p.resourceUrl = resourceUrl;
		p.srcUrl = resourceUrl;
		p.title = title;
		p.description = description;
		p.pic = pic;
		p.charset = '';
		
		var prm = [];
		for (var i in p) {
			if (p[i])
				prm.push(i + '=' + encodeURIComponent(p[i]));
		}
		
		var baseurl = "http://widget.renren.com/dialog/share";	
		var url = baseurl + '?' + prm.join('&');
		var maxLgh = (isIE ? 2048 : 4100); 
		var wa = "width=700,height=650,left=0,top=0,resizable=yes,scrollbars=1, left=" + (screen.width-700)/2 + ',top=' + (screen.height-650)/2;
		
		if (url.length > maxLgh) {
			window.open('about:blank', 'fwd', wa);
			postTarget({
				url : baseurl,
				target : 'fwd',
				params : p
			});
		} else {
			window.open(url, 'fwd', wa);
		}
		
	};
	if (/Firefox/.test(navigator.userAgent))
	setTimeout(a, 0);
	else
	a();
}
