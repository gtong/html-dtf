<?php
class accessToken
{
	function get_access_token($appid, $secret){
		$url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . $appid . "&secret=" . $secret;
		$json = $this->http_request_json($url);
		$data = json_decode($json, true);
		if ($data['access_token']){
			return $data['access_token'];
		} else{
			return $data['errmsg'];
		}
	}

	private function http_request_json($url){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
}

$appid = "wx46b9a8a96055f249";
$secret = "fd0a3619aed8e87fef87f048541c62d0";
$access_token = new accessToken();
$result = $access_token->get_access_token($appid, $secret);
echo $result;
?>
