<?php
class IndexAction extends Action {
    public function index(){
		$this->display();
    }

	public function comment(){
		$this->referer = $_SERVER["HTTP_REFERER"];
		$this->display();
	}
}
