<?php
$menu = new createMenu();
$menu->create_menu();

class createMenu
{
	private $accessToken = "I-R3IKTk9X5Y6VJc6QjNIEbuHAJTjW6qCGBVkYb1aSNs0mB61VtTzLlj78HogD4syKo8U_mRIBLZ8WKKq2xn65b0OBk8wee2J2CSwmLdynl_ppBQJ0jtpkn73wGgVaqobsCEgyjxBNrpphTnJQwRjw";

	function create_menu(){
		$url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" . $this->accessToken;	
		$data = '
				{
					"button":[
					{
						"type":"view",
						"name":"Home",
						"url":"http://42.96.174.106/app/"
					},
					{
						"type":"view",
						"name":"About Author",
						"url":"http://42.96.174.106/app/index.php/about"
					}]
				}';

		$this->post($url, $data);
	}

	private function post($url, $data){
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		$tempInfo = curl_exec($curl);
		if(curl_errno($curl)){
			echo 'Error' . curl_error($curl);
		}
		curl_close($curl);
		return $tempInfo;
	}
}
?>
